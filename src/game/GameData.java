package game;

public class GameData {
	private Board board;
	private boolean team; //0 will be opponent 1 will be the player
	private int value; 
	
	public GameData(Board board, boolean team, int value){
		this.board = new Board();
		this.team = team;
		this.value = value;
	}
	
	//getters
	public Board getBoard(){
		return board;
	}
	
	public boolean getTeam(){
		return team;
	}
	
	//this will probably be the only one we need..
	public int getValue(){
		return value;
	}
	
}
