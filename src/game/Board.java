package game;
import static java.lang.Math.*;
public class Board {
	private int[][] board;
	private int turn;
	
	public Board(){
		board = new int[3][3];
		
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				this.board[i][j] = 0;
			}
		}
		
	}
	
	//public Board(int[][] initConfig){
	//	this.board = initConfig;
	//}
	
	//public methods
	int[][] getBoard(){
		return board;
	}
	
	
	//TODO: maybe print out the x/y coordinates 
	void printBoard(){
		for(int j = 0; j < 3; j++){
			for(int i = 0; i < 3; i++){
				if(i < 2){
					if(board[i][j] == 1)	
						System.out.print(" X " + "|");
					else if(board[i][j] == -1)
						System.out.print(" O " + "|");
					else 
						System.out.print("   " + "|");
				}else{
					if(board[i][j] == 1)
						System.out.print(" X ");	
					else if(board[i][j] == -1)
						System.out.print(" O ");
					else
						System.out.print("   ");
				}
				
			}
			if(j < 2){
				System.out.println();
				System.out.print("--- --- ---");
				System.out.println();
			}
		}
		System.out.println();
		
	}
	void clearBoard(){
		board = new int[][]{{0,0,0},{0,0,0},{0,0,0}}; //have to make a new board
	}
	
	
	boolean set(int x,int y, int team){
		//check if input is valid
		if(x > 3 || x < 0 || y > 3 || y < 0 || abs(team) != 1){
			return false;
		}
		//check if board is empty there
		else if(board[x][y] != 0) {
			return false; 
		}
		//make the move
		else { 
			board[x][y] = team;
			return true;
		}	
	}
	
	
	
	void setTurn(int newTurn){
		this.turn = newTurn;
	}
	
	int getTurn(){
		return this.turn;
	}
	
	//returns the winning team (1 or -1) if there is one. If not returns 0, if it's 
	//a cat's game returns 2
	int checkVictory(){
		int rowCheck = 0;
		int columnCheck = 0;
		int zeroCheck = 0;
		//will check rows and columns 
		for(int i = 0; i < 3; i++){
			rowCheck = 0;
			columnCheck = 0;
			zeroCheck = 0;
			for(int j = 0; j < 3; j++){
				rowCheck += board[i][j];
				columnCheck += board[j][i];
				
				if (board[i][j] == 0){
					zeroCheck = 1;
				}
			}
			if(abs(rowCheck) == 3){     //if check == 3 or -3 then on team has three in a row
				return rowCheck/3;      //check/3 will return -1 or 1, which is the team with
			}if(abs(columnCheck) == 3){ //the victory
				return columnCheck/3;    
			}
			//check the diagonals manually, since there's only two
			if(abs(board[0][0] + board[1][1] + board[2][2]) == 3){
				return (board[0][0] + board[1][1] + board[2][2])/3;
			}if(abs(board[0][2] + board[1][1] + board[2][0]) == 3){
				return (board[0][2] + board[1][1] + board[2][0])/3;
			}
			
			if(zeroCheck == 0){
				return 2;
			}
		}
		
		return 0;
	}
		
}
