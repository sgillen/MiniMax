package game;

import java.util.ArrayList;
import java.util.List;

public abstract class MiniMaxTree<T> {
	protected Node<T> root;
	
	public MiniMaxTree(){
		//Please don't call this use the constructor that actucally has an argument 
		root = null;
	}
	
	public MiniMaxTree(T data) {
		root = new Node<T>(data, null); //the root obviously has no parent
		this.fillTree(root); 
		//root.data = data;
		//root.children = new ArrayList<Node<T>>();
	}
	
	protected static class Node<T>{
		protected T data;
		protected Node<T> parent;
		protected List<Node<T>> children;
		
		protected Node(T data, Node<T> parent){
			this.data = data;
			this.parent = parent;
			this.children = new ArrayList<Node<T>>();
		}
		
	}
	
	protected void addChild(T data, Node<T> parent){
		//May be better to include this as a field..
		//int i;
		//for(i = 0; parent.children.get(i) != null; i++);
		Node<T> newNode = new Node<T>(data, parent); 
		parent.children.add(newNode);
		
	}
	
	public Node<T> getRoot(){
		return root;
	}
	public Node<T> getParent(Node<T> node){
		return node.parent;
	}
	public List<Node<T>> getChildren(Node<T> node){
		return node.children;
	}
	public T getData(Node<T> node){
		return node.data;
	}
	
	protected abstract void fillTree(Node<T> node);
}
