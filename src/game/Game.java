package game;
import java.util.Scanner;


public class Game {
	static Scanner in = new Scanner(System.in);
	static Board theBoard = new Board();
    static AI opponent = new AI();	
	
	public static void main(String[] args) {
		theBoard.clearBoard();

		//this call generates the entire game tree, for more complicated games we won't get away with this.
		opponent.makeGameTree(theBoard);
	
		int x, y;
		theBoard.setTurn(1);
		boolean done = false;
		String move;
				
		while(!done){
			
			theBoard.printBoard();
			System.out.println("player " + theBoard.getTurn() + ", enter the coordinates of the space you want to mark (x,y) : ");
			if(theBoard.getTurn() == -1){
				move = opponent.makeMove(theBoard.getBoard());
			}else{
				move = in.next();
			}
			
			if(move.matches("\\d,\\d")){ 
				x = Character.getNumericValue(move.charAt(0));
				y = Character.getNumericValue(move.charAt(2));
				System.out.println(x + " " + y);
				if(theBoard.set(x,y,theBoard.getTurn())){
					theBoard.setTurn(-1*theBoard.getTurn());
				}
			}
			
			if(theBoard.checkVictory() == 1){
				System.out.println("player 1 you win!");
				break;
			}
			else if(theBoard.checkVictory() == -1){
				System.out.println("player -1 you win!");
				break;
			}else if(theBoard.checkVictory() == 2){
				System.out.println("Cat's Game!");
				break;
			}
			
		}//end while loop
	}
	
}
