package game;

public class AI {
	
	private MiniMaxTree<GameData> gameTree;
	
	String  makeMove(int[][] board){
		System.out.println("this is a test\n");
		return ("0,0");
		
	}
	
	//in tic tac toe we can get away with generating the full game tree, in general that probably won't fly
	void makeGameTree(Board board){
		int rootValue = TicTacTree.assignValue(board.getBoard());
		GameData rootData = new GameData(board, true , rootValue);
		gameTree = new TicTacTree(rootData);
		gameTree.fillTree(gameTree.root);
		
	}
	
}
