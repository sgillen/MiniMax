package game;

import static java.lang.Math.*;


public class TicTacTree extends MiniMaxTree<GameData> {

	public TicTacTree(GameData data) {
		root = new Node<GameData>(data, null); // the root obviously has no
												// parent
		this.fillTree(root);
		// root.data = data;
		// root.children = new ArrayList<Node<T>>();
	}

	// assigns value and lets whoever is calling it decide if it needs to
	// maximize or
	// minimize.
	public static int assignValue(int[][] board) {
		int rowVal = 0;
		int columnVal = 0;
		int zeroVal = 0;
		int diagonalVal1 = 0;
		int diagonalVal2 = 0;
		int value = 0; // that is the 'score' we assign this particular board

		// will check rows and columns
		for (int i = 0; i < 3; i++) {
			rowVal = 0;
			columnVal = 0;
			zeroVal = 0;
			for (int j = 0; j < 3; j++) {
				rowVal += board[i][j];
				columnVal += board[j][i];
				if (board[i][j] == 0) {
					zeroVal = 1;
				}
			}

			// I'm actually not entirely sure this will be optimal in all cases,since
			// values of 1 can correspond to full rows/columns/whatever but I think it
			// will still give results I want to see. 
			switch (rowVal) {
			case  1:value += 1;
			case  2:value += 10;
			case  3:value += 1000;
			case -1:value -= 1;
			case -2:value -= 10;
			case -3:value -= 1000;

			}

			switch (columnVal) {
			case  1:value += 1;
			case  2:value += 10;
			case  3:value += 1000;
			case -1:value -= 1;
			case -2:value -= 10;
			case -3:value -= 1000;

			}
			
			//we're still in the outside for loop 
			diagonalVal1 += board[i][i]; 
			diagonalVal2 += board[i][i];
			/*
			//I wanted to somehow distinguish boards which have already been won, but I
			 * suppose the game apparatus will take care of that
			if (zeroVal == 0) {
				return 0; 
			}
			*/
		}
		
		
			switch(diagonalVal1){
			case  1: value+=1;
			case  2: value+=10;
			case  3: value+=1000;
			case -1: value-=1;
			case -2: value-=10;
			case -3: value-=1000;
			
			}
			
				switch(diagonalVal2){
			case  1: value+=1;
			case  2: value+=10;
			case  3: value+=1000;
			case -1: value-=1;
			case -2: value-=10;
			case -3: value-=1000;
			
			}

		return value;
	}
	
	
	

	protected void fillTree(Node<GameData> node) {
		if (node.data.getBoard().checkVictory() != 0) { // anything but 0 means
														// there are no more
														// moves to me made, so
														// we're done
			return;
		} else {
			int team = node.data.getBoard().getTurn();

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					node.data.getBoard().set(i, j, team);
				}
			}

		}

		for (int i = 0; i < node.children.size(); i++) {
			fillTree(node.children.get(i));
		}
	}

}
